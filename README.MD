## PROJECT TRACKER
This is a project and task manager application. It is composed of projects, tasks, and the ability to login, logout, and sign up for the application.

## Functionality
- Create an account to access PROJECT MANAGER features
- Log in to your account to view your projects
- Click on a project to view the tasks
- Mark a project as complete
- create a new task


## Project Initialization
To fully enjoy this application on your local machine, please make sure to follow these steps:
1. Clone the repository down to your local machine
2. CD into the new project directory
3. Activate a virtual environment
    source ./.venv/bin/activate  # macOS
    ./.venv/Scripts/Activate.ps1 # Windows
4. Update pip
    python -m pip install --upgrade pip
5. Install dependencies from requirements.txt
pip install -r requirements.txt
6. Run your development server
    python manage.py runserver
